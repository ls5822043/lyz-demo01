package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("capital")
public class CapitalEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private Long targetId;

    private Integer type;//类型 0 充值 1下单 2提现

    private BigDecimal balanceDetails;//余额总计

    private BigDecimal fundDetails;//余额变动明细

    private String tips;//说明

    private Date creatTime;

}
