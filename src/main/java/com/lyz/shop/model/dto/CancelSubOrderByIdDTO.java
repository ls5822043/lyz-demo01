package com.lyz.shop.model.dto;

import lombok.Data;

@Data
public class CancelSubOrderByIdDTO {

    private Long id;

    private Integer state;

    private Integer source;
}
