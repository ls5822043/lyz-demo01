package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("cart")
public class CartEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private Long productId;


    private Long number;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;

    @TableField(exist = false)
    private ProductEntity productEntity;

}
