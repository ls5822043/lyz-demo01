package com.lyz.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.CollectMapper;
import com.lyz.shop.model.CollectEntity;
import com.lyz.shop.service.CollectService;
import org.springframework.stereotype.Service;

@Service
public class CollectServiceImpl extends ServiceImpl<CollectMapper, CollectEntity> implements CollectService {
    @Override
    public Long countByUserIdAndTargetIdAndType(Long userId, Long targetId, Integer type) {
        long count = count(new LambdaQueryWrapper<CollectEntity>()
                .eq(CollectEntity::getUserId, userId)
                .eq(CollectEntity::getTargetId, targetId)
                .eq(CollectEntity::getType, type)
                .last(" limit 1")
        );
        return count;
    }
}
