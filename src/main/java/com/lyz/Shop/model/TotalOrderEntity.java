package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("total_order")
public class TotalOrderEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String orderNo;

    private String province;//省

    private String city;//市

    private Long userId;

    private String area;

    private String phone;

    private String name;

    private String address;

    private Long totalCount;

    private BigDecimal totalPrice;

    private Integer state;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date updateTime;


}
