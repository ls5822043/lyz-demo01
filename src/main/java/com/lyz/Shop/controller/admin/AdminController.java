package com.lyz.shop.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.*;
import com.lyz.shop.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private UserService userService;
    @Autowired
    private BusinessService businessService;
    @Autowired
    private SubOrderService subOrderService;
    @Autowired
    private InformationService informationService;

    //新增管理员
    @PostMapping("reAdmin")
    public Base reAdmin(@RequestBody AdminEntity admin){

        if (StringUtils.isEmpty(admin.getAccountNumber())||StringUtils.isEmpty(admin.getName())||StringUtils.isEmpty(admin.getPassword())||
        StringUtils.isEmpty(admin.getPhone())||StringUtils.isEmpty(admin.getSex())){
            return Base.fail(-1,"传入参数不能为空");
        }


        //通过昵称或电话或账号 判断是否已经注册过
        LambdaQueryWrapper<AdminEntity> nickNamewrapper = new LambdaQueryWrapper<>();
        nickNamewrapper.eq(AdminEntity::getNickName,admin.getNickName());
        long count = adminService.count(nickNamewrapper);
        if (count>0){
            return Base.fail(-1,"昵称已注册");
        }
        LambdaQueryWrapper<AdminEntity> phoneWrapper = new LambdaQueryWrapper<>();
        phoneWrapper.eq(AdminEntity::getPhone,admin.getPhone());
        long phone = adminService.count(nickNamewrapper);
        if (phone>0){
            return Base.fail(-1,"电话已注册");
        }
        LambdaQueryWrapper<AdminEntity> accountWrapper = new LambdaQueryWrapper<>();
        accountWrapper.eq(AdminEntity::getAccountNumber,admin.getAccountNumber());
        long account = adminService.count(accountWrapper);
        if (account>0){
            return Base.fail(-1,"账号已注册");
        }


        //注册保存
        //状态默认为1启用   0禁用
        admin.setState(0);
        boolean save = adminService.save(admin);
        if (!save){
            return Base.fail(-1,"注册失败");
        }
        return Base.sure("注册成功");
    }

    //管理员登录
    //根据账号和密码
    @GetMapping("adminLogin")
    public Base adminLogin(String accountNumber,String password){
        if (StringUtils.isEmpty(accountNumber)||StringUtils.isEmpty(password)){
            return Base.fail(-1,"请输入账号和密码");
        }
        //判断账号密码是否为空
        LambdaQueryWrapper<AdminEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AdminEntity::getAccountNumber,accountNumber);
        wrapper.eq(AdminEntity::getPassword,password);
        AdminEntity entity = adminService.getOne(wrapper);
        if (entity==null){
            return Base.fail(-1,"账号或密码错误");
        }
        return Base.data(entity);
    }


    //修改管理员信息
    @PutMapping("putAdminById")
    public Base putAdminById(@RequestBody AdminEntity adminEntity){
        if (StringUtils.isEmpty(adminEntity.getId())){
            return Base.fail(-1,"id不能为空");
        }
        //判断管理员信息是否为空
        AdminEntity byId = adminService.getById(adminEntity.getId());
        if (byId==null){
            return Base.fail(-1,"用户信息不存在");
        }

        //判断要修改的电话是否被占用
        //并且有一个电话是自己的 所以要判断那个电话是否是自己的
        //通过id不等于来确定不是自己的电话
        LambdaQueryWrapper<AdminEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AdminEntity::getPhone,adminEntity.getPhone());
        wrapper.ne(AdminEntity::getId,adminEntity.getId());
        long count = adminService.count(wrapper);
        if (count>0){
            return Base.fail(-1,"电话已占用");
        }

        //只能修改状态 电话 密码
        adminEntity.setAccountNumber(null);
        adminEntity.setNickName(null);
        adminEntity.setName(null);
        adminEntity.setSex(null);
        adminEntity.setPhone(adminEntity.getPhone());

        //保存修改
        boolean update = adminService.updateById(adminEntity);
        if (!update){
            return Base.fail(-1,"修改失败");
        }
        return Base.sure("修改成功");
    }

    //管理员查看用户列表
    @GetMapping("adminFindUser")
    public Base adminFindUser(){
        List<Map<String, Object>> maps = userService.listMaps();
        return Base.data(maps);
    }

    //查看商家列表
    @GetMapping("adminFindBusiness")
    public Base adminFindBusiness(){
        List<Map<String, Object>> maps = businessService.listMaps();
        return Base.data(maps);
    }

    //通过类别展示商家列表
    @GetMapping("findBusinessByType")
    public Base findBusinessByType(@RequestParam(value = "type") String type){
        if (StringUtils.isEmpty(type)){
            return Base.fail(-1,"请输入商家类别");
        }
        LambdaQueryWrapper<BusinessEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(BusinessEntity::getType,type);
        List<Map<String, Object>> maps = businessService.listMaps(wrapper);
        return Base.data(maps);
    }

    //通过电话或订单编号查看订单
    @GetMapping("adminSubOrderByPhoneOrOrderNo")
    public Base adminSubOrderByPhoneOrOrderNo(String phone,String orderNo){
        if (StringUtils.isEmpty(phone)&& StringUtils.isEmpty(orderNo)){
            return Base.fail(-1,"请输入订单号或电话");
        }
        LambdaQueryWrapper<SubOrderEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SubOrderEntity::getOrderNo,orderNo).or()
                .eq(SubOrderEntity::getPhone,phone);
        SubOrderEntity one = subOrderService.getOne(wrapper);
        if (one==null){
            return Base.fail(-1,"电话或订单号错误");
        }
        return Base.data(one);
    }

    //修改用户状态
    @PutMapping("adminPutUserStateById")
    public Base adminPutUserStateById(@RequestBody UserEntity userEntity){
        if (StringUtils.isEmpty(userEntity.getId())){
            return Base.fail(-1,"请输入id");
        }
        //查询用户是否为空
        UserEntity byId = userService.getById(userEntity.getId());
        if (byId==null){
            return Base.fail(-1,"用户不存在");
        }
        //修改用户状态
        userEntity.setState(userEntity.getState());
        boolean update = userService.updateById(userEntity);
        if (!update){
            return Base.fail(-1,"修改失败");
        }
        return Base.sure("修改成功");
    }

    //展示资讯列表
    @GetMapping("findInformation")
    public Base findInformation(){
        LambdaQueryWrapper<InformationEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.select(InformationEntity::getHeadline,InformationEntity::getAuthor);
        List<Map<String, Object>> maps = informationService.listMaps(wrapper);
        return Base.data(maps);
    }
}
