package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("recharge")
public class RechargeEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private Long rechargeBalance;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;
}
