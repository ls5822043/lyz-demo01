package com.lyz.shop.service.impl;
;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.InformationMapper;
import com.lyz.shop.model.InformationEntity;
import com.lyz.shop.service.InformationService;
import org.springframework.stereotype.Service;

@Service
public class InformationServiceImpl extends ServiceImpl<InformationMapper,InformationEntity> implements InformationService {

}
