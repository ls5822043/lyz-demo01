package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("Comment")
public class CommentEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private Long subOrderId;

    private Integer mark;//星级

    private String content;//评价内容

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;
}
