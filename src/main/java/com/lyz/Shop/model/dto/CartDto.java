package com.lyz.shop.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class CartDto {

    private Long id;

    private Long userId;

    private Long productId;

    private Long number;

    private Integer source;

}
