package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.TotalOrderEntity;

public interface TotalOrderService extends IService<TotalOrderEntity> {

}
