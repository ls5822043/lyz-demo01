package com.lyz.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.AdminMapper;
import com.lyz.shop.model.AdminEntity;
import com.lyz.shop.service.AdminService;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, AdminEntity>implements AdminService {


}
