package com.lyz.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyz.shop.model.CommentEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommentMapper extends BaseMapper<CommentEntity> {
}
