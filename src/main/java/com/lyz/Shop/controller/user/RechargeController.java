package com.lyz.shop.controller.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.CapitalEntity;
import com.lyz.shop.model.RechargeEntity;
import com.lyz.shop.model.UserEntity;
import com.lyz.shop.model.dto.DelRechargeDTO;
import com.lyz.shop.service.CapitalService;
import com.lyz.shop.service.RechargeService;
import com.lyz.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("recharge")
public class RechargeController {
    //充值表
    @Autowired
    private RechargeService rechargeService;
    @Autowired
    private UserService userService;
    @Autowired
    private CapitalService capitalService;

    //充值
    @PostMapping("recharge")
    public Base recharge(@RequestBody RechargeEntity recharge){
        if (recharge.getUserId()==null||recharge.getRechargeBalance()==null){
            return Base.fail(-1,"传入的参数不能为空");
        }
       //查询用户
        LambdaQueryWrapper<UserEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserEntity::getId,recharge.getUserId());
        queryWrapper.last("limit 1");
        UserEntity userServiceById = userService.getOne(queryWrapper);
        if (userServiceById!=null){
            //将余额进行叠加
            BigDecimal balance = userServiceById.getBalance();
            Long rechargeBalance = recharge.getRechargeBalance();

            BigDecimal decimal = new BigDecimal(rechargeBalance);
            BigDecimal value = BigDecimal.valueOf(rechargeBalance);
            userServiceById.setBalance(value.add(balance));

           //一元等于一积分
            Long reBalance = recharge.getRechargeBalance();
            Long integral = userServiceById.getIntegral();
            userServiceById.setIntegral(integral+reBalance);


            if (userServiceById.getIntegral()>=5000){
                userServiceById.setMember("白银会员");
            }
            if (userServiceById.getIntegral()>=10000){
                userServiceById.setMember("黄金会员");
            }
            if (userServiceById.getIntegral()>=15000){
                userServiceById.setMember("铂金会员");
            }
            if (userServiceById.getIntegral()>=30000){
                userServiceById.setMember("砖石会员");
            }
            if (userServiceById.getIntegral()>=200000){
                userServiceById.setMember("王者会员");
            }
            boolean update = userService.updateById(userServiceById);
        }


        //保存
        recharge.setCreatTime(new Date());
        boolean save = rechargeService.save(recharge);
        if (!save){
            return Base.fail(-1,"充值失败");
        }

        //资金明细进行变动
        //新增订单时进行保存
        Long rechargeBalance = recharge.getRechargeBalance();//充值金额
        BigDecimal bigDecimal = new BigDecimal(rechargeBalance);
        BigDecimal balance = userServiceById.getBalance();

        CapitalEntity capitalEntity = new CapitalEntity();
        capitalEntity.setTargetId(recharge.getId());
        capitalEntity.setType(0);
        capitalEntity.setTips("充值");
        capitalEntity.setUserId(recharge.getUserId());
        capitalEntity.setFundDetails(balance.subtract(bigDecimal));//总金额-充值金额=原金额
        capitalEntity.setBalanceDetails(balance);//总余额
        capitalEntity.setCreatTime(new Date());
        capitalService.save(capitalEntity);

        return Base.sure("充值成功");
    }

    //查看充值记录
    @GetMapping("findRechargeByUserId")
    public Base findRechargeByUserId(@RequestParam(value = "userId") Long userId){
        LambdaQueryWrapper<RechargeEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(RechargeEntity::getUserId,userId);
        List<RechargeEntity> list = rechargeService.list(wrapper);
        return Base.data(list);
    }

    //删除充值记录
    @DeleteMapping("delRecharge")
    public Base delRecharge(DelRechargeDTO dto) {
        if (dto.getSource() == null) {
            return Base.fail(-1, "请选择一个操作方式");
        }
        if (dto.getSource()==0){
            //根据id删除
            boolean remove = rechargeService.removeById(dto.getId());
            if (!remove){
                return Base.fail(-1,"单个操作失败");
            }
            return Base.sure("单个操作成功");
        }else {
            //批量操作
            LambdaQueryWrapper<RechargeEntity> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(RechargeEntity::getUserId,dto.getUserId());
            boolean remove = rechargeService.remove(wrapper);
            if (!remove){
                return Base.fail(-1,"批量删除失败");
            }
        }
        return Base.fail(-1,"批量操作成功");
    }
}
