package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("cash")
public class CashEntity {
    @TableId(type = IdType.AUTO)
    private  Long id;

    private Long userId;

    private BigDecimal cashBalance;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date CreatTime;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date receiptTime;
}
