package com.lyz.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.CartMapper;
import com.lyz.shop.model.CartEntity;
import com.lyz.shop.service.CartService;
import org.springframework.stereotype.Service;


@Service
public class CartServiceImpl extends ServiceImpl<CartMapper, CartEntity> implements CartService {

}
