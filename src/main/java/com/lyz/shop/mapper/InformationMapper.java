package com.lyz.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyz.shop.model.InformationEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InformationMapper extends BaseMapper<InformationEntity> {
}
