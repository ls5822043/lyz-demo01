package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.AddressEntity;


public interface AddressService extends IService<AddressEntity> {
}
