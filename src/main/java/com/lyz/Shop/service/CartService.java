package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.CartEntity;

public interface CartService extends IService<CartEntity> {
}
