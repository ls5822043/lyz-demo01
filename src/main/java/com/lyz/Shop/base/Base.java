package com.lyz.shop.base;

import lombok.Data;

@Data
public class Base <T>{

    private int code=0;

    private String msg;

    private T data;

    public Base() {
    }

    public Base(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Base(String msg) {
        this.msg = msg;
    }

    public Base(T data) {
        this.data = data;
    }

    public static Base sure(String msg){
        return new Base(msg);
    }
    public static Base fail(int code,String msg){
        return new Base(code,msg);
    }
    public static <T>Base data(T data){
        return new Base(data);
    }
}
