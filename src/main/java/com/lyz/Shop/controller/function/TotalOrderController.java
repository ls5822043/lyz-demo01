package com.lyz.shop.controller.function;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.*;
import com.lyz.shop.model.dto.AddSubOrderDTO;
import com.lyz.shop.model.dto.OrderItemDTO;
import com.lyz.shop.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("totalOrder")
public class TotalOrderController {

    @Autowired
    private TotalOrderService totalOrderService;
    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;
    @Autowired
    private SubOrderService subOrderService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private CartService cartService;
    @Autowired
    private CapitalService capitalService;


    @PostMapping("createOrder")
    @Transactional
    public Base createOrder(@RequestBody AddSubOrderDTO dto){
        if(dto.getUserId()==null||dto.getAddressId()==null|| ObjectUtils.isEmpty(dto.getOrderItems())){
            return Base.fail(-1,"参数错误");
        }

        Long addressId = dto.getAddressId();
        Long userId = dto.getUserId();
        List<Long> cardIds=null;
        if(!ObjectUtils.isEmpty(dto.getCardIds())){
            cardIds = dto.getCardIds();
        }
        List<OrderItemDTO> orderItems = dto.getOrderItems();

        UserEntity userEntity = userService.getById(userId);
        if(userEntity==null){
            return Base.fail(-1,"用户不存在");
        }
        Date date = new Date();
        //收货地址
        AddressEntity addressEntity = addressService.getById(addressId);
        if(addressEntity==null){
            return Base.fail(-1,"收货地址不存在");
        }
        try {
            //初始化总订单
            TotalOrderEntity totalOrderEntity = new TotalOrderEntity();
            totalOrderEntity.setUserId(userId);
            totalOrderEntity.setName(addressEntity.getName());
            totalOrderEntity.setPhone(addressEntity.getPhone());
            totalOrderEntity.setProvince(addressEntity.getProvince());
            totalOrderEntity.setCity(addressEntity.getCity());
            totalOrderEntity.setArea(addressEntity.getArea());
            totalOrderEntity.setState(1);
            totalOrderEntity.setTotalPrice(BigDecimal.valueOf(0));
            totalOrderEntity.setTotalCount(0L);
            totalOrderEntity.setCreatTime(date);
            totalOrderEntity.setUpdateTime(date);
            totalOrderEntity.setOrderNo("DD"+date.getTime());
            totalOrderService.save(totalOrderEntity);

            List<SubOrderEntity> items=new ArrayList<>();
            for (int i = 0; i < orderItems.size(); i++) {
                OrderItemDTO item = orderItems.get(i);
                ProductEntity productEntity = productService.getById(item.getProductId());
                if(productEntity!=null){
                    //判断库存
                    Long stock = productEntity.getStock();
                    if(stock<=item.getNumber()){
                        throw new RuntimeException("库存不足");
                    }
                    BigDecimal price = productEntity.getPrice();
                    BigDecimal total = price.multiply(new BigDecimal(item.getNumber()));
                    totalOrderEntity.setTotalPrice(totalOrderEntity.getTotalPrice().add(total));
                    totalOrderEntity.setTotalCount(totalOrderEntity.getTotalCount()+item.getNumber());

                    //构造子订单
                    SubOrderEntity subOrderEntity=new SubOrderEntity();
                    subOrderEntity.setTotalOrderId(totalOrderEntity.getId());
                    subOrderEntity.setProductId(item.getProductId());
                    subOrderEntity.setBusinessId(productEntity.getBusinessId());
                    subOrderEntity.setOrderNo(totalOrderEntity.getOrderNo());
                    subOrderEntity.setPrice(productEntity.getPrice());
                    subOrderEntity.setNumber(Long.valueOf(item.getNumber()));
                    subOrderEntity.setCreatTime(date);
                    subOrderEntity.setUpdateTime(date);
                    items.add(subOrderEntity);

                    productEntity.setSale(productEntity.getSale()+item.getNumber());
                    productService.updateById(productEntity);
                }
            }
            //判断余额
            BigDecimal balance = userEntity.getBalance();
            if(balance.compareTo(totalOrderEntity.getTotalPrice())<0){
                throw new RuntimeException("余额不足");
            }
            subOrderService.saveBatch(items);
            totalOrderService.updateById(totalOrderEntity);

            //删除购物车
            if(cardIds!=null){
                cartService.removeByIds(cardIds);
            }
            //修改用户余额
            userEntity.setBalance(userEntity.getBalance().subtract(totalOrderEntity.getTotalPrice()));
            userService.updateById(userEntity);

            //添加余额变动明细
            CapitalEntity capitalEntity=new CapitalEntity();
            capitalEntity.setUserId(userId);
            capitalEntity.setBalanceDetails(userEntity.getBalance());
            capitalEntity.setFundDetails(totalOrderEntity.getTotalPrice());
            capitalEntity.setTargetId(totalOrderEntity.getId());
            capitalEntity.setCreatTime(date);
            capitalEntity.setType(1);
            capitalEntity.setTips("购买商品");
            capitalService.save(capitalEntity);

        }catch (Exception e){
            throw new RuntimeException("下单失败");
        }
        return Base.sure("提交成功");
    }


    //发货
    @GetMapping("sendBag/{id}")
    public Base sendBag(@PathVariable("id")Long id){
        TotalOrderEntity orderEntity = totalOrderService.getById(id);
        if(orderEntity==null){
            return Base.fail(-1,"没有该订单");
        }
        if(orderEntity.getState()!=1){
            return Base.fail(-1,"该订单不可发货");
        }
        orderEntity.setState(2);
        totalOrderService.save(orderEntity);
        return Base.sure("发货成功");
    }



}
