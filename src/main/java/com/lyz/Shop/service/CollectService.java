package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.CollectEntity;

public interface CollectService extends IService<CollectEntity> {

    Long countByUserIdAndTargetIdAndType(Long userId,Long targetId,Integer type);
}
