package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("user")
public class UserEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private String nickName;

    private String accountCount;//账号

    private String password;

    private BigDecimal balance;

    private Integer sex;

    private String phone;

    private String address;//地址

    private String introduce;//简介

    private Integer state;

    private String member;//会员

    private Long integral;//积分

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthdayTime;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;
}
