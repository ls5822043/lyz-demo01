package com.lyz.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.RefundMapper;
import com.lyz.shop.model.RefundEntity;
import com.lyz.shop.service.RefundService;
import org.springframework.stereotype.Service;

@Service
public class RefundServiceImpl extends ServiceImpl<RefundMapper, RefundEntity> implements RefundService {
}
