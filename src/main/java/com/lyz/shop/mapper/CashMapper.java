package com.lyz.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyz.shop.model.CashEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CashMapper extends BaseMapper<CashEntity> {
}
