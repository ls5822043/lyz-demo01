package com.lyz.shop.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NonNull;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class AddSubOrderDTO {




    private Long userId;


    private Long addressId;


    private List<Long> cardIds;

    private List<OrderItemDTO> orderItems;



}
