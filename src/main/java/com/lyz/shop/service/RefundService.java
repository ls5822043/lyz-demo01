package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.RefundEntity;

public interface RefundService extends IService<RefundEntity> {
}
