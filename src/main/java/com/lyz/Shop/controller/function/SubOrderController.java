    package com.lyz.shop.controller.function;

    import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
    import com.lyz.shop.base.Base;
    import com.lyz.shop.model.*;
    import com.lyz.shop.model.dto.AddSubOrderDTO;
    import com.lyz.shop.model.dto.PutOrderAddressDTO;
    import com.lyz.shop.service.*;
    import org.springframework.beans.BeanUtils;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.util.StringUtils;
    import org.springframework.validation.annotation.Validated;
    import org.springframework.web.bind.annotation.*;

    import java.math.BigDecimal;
    import java.util.Date;
    import java.util.List;
    import java.util.Map;

    @RestController
    @RequestMapping("subOrder")
    public class SubOrderController {

        @Autowired
        private SubOrderService subOrderService;
        @Autowired
        private ProductService productServer;
        @Autowired
        private TotalOrderService totalOrderService;
        @Autowired
        private CartService cartService;
        @Autowired
        private UserService userService;
        @Autowired
        private CapitalService capitalService;
        @Autowired
        private AddressService addressService;

        //新增订单明细






        //修改订单地址
        @PutMapping("putAddressById")
        public Base putAddressById(@RequestBody PutOrderAddressDTO dto) {

            return Base.sure("修改成功");
        }
    }
