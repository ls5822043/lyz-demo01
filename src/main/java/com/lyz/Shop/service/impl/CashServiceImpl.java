package com.lyz.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.CashMapper;
import com.lyz.shop.model.CashEntity;
import com.lyz.shop.service.CashService;
import org.springframework.stereotype.Service;

@Service
public class CashServiceImpl extends ServiceImpl<CashMapper, CashEntity> implements CashService {
}
