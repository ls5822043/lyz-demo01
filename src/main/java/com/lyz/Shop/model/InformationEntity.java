package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("information")
public class InformationEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long businessId;

    private String author;//作者

    private String relation;//联系方式

    private String headline;//标题

    private String content;//内容

    private String picture;//图片

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date updateTime;
}
