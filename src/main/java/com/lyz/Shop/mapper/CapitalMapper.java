package com.lyz.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyz.shop.model.CapitalEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CapitalMapper extends BaseMapper<CapitalEntity> {
}
