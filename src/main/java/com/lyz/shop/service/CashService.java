package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.CashEntity;

public interface CashService extends IService<CashEntity> {
}
