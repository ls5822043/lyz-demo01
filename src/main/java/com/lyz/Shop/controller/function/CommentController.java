package com.lyz.shop.controller.function;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.*;
import com.lyz.shop.model.dto.FindCommentDTO;
import com.lyz.shop.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private SubOrderService subOrderService;
    @Autowired
    private ProductService productServer;

    //新增客户评价
    @PostMapping("addUserComment")
    public Base addUserComment(@RequestBody CommentEntity comment) {
        if (comment.getUserId() == null || comment.getSubOrderId() == null || comment.getMark() == null) {
            return Base.fail(-1, "参数不能为空");
        }
        //根据用户id 订单Id判断订单存在
        LambdaQueryWrapper<SubOrderEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SubOrderEntity::getUserId, comment.getUserId());
        wrapper.eq(SubOrderEntity::getId, comment.getSubOrderId());
        SubOrderEntity one = subOrderService.getOne(wrapper);
        if (one == null) {
            return Base.fail(-1, "订单不存在");
        }

        //通过查询sid这条记录判断商品是否已经评价
        LambdaQueryWrapper<CommentEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CommentEntity::getSubOrderId, comment.getSubOrderId());
        long counts = commentService.count(queryWrapper);
        if (counts > 0) {
            return Base.fail(-1, "订单已评价过");
        }

        //新增评价
        comment.setCreatTime(new Date());
        if (comment.getMark() < 1 || comment.getMark() > 5) {
            return Base.fail(-1, "星级只能在1-5之间");
        }
        if (comment.getContent()==null && comment.getMark()>=1 && comment.getMark()<=3){
            comment.setContent("用户觉得不好 并且默默的给了个踩");
        }
        if (comment.getContent()==null && comment.getMark()>=4 && comment.getMark()<=5){
            comment.setContent("用户觉得很好 并且默默的给了个赞");
        }
        boolean save = commentService.save(comment);
        if (!save) {
            return Base.fail(-1, "评价失败");
        }
        return Base.sure("评价成功");
    }

    //查看全部评论
    //通过星级查看对应星级的评价
    @GetMapping("findComment")
    public Base findComment(FindCommentDTO dto){
        if (dto.getSource()==null){
            return Base.fail(-1,"source不能为空");
        }
        if (dto.getSource()==0){
            //查看全部评价
            List<CommentEntity> list = commentService.list();
            return Base.data(list);
        }else {
            //根据星级查看评价
            List<CommentEntity> list = commentService.list(new LambdaQueryWrapper<CommentEntity>()
            .eq(CommentEntity::getMark,dto.getMark())
            );
            return Base.data(list);
        }
    }

    //删除评价
    @DeleteMapping("delCommentById")
    public Base delCommentById(Long id){
        if (StringUtils.isEmpty(id)){
            return Base.fail(-1,"id不能为空");
        }
        boolean b = commentService.removeById(id);
        if (!b){
            return Base.fail(-1,"评价已删除");
        }
        return Base.sure("删除成功");
    }


}
