package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.Date;

@Data
@TableName("collect")
public class CollectEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private Integer type;

    private Long targetId;//目标id

    @TableField(exist = false)
    private Boolean source;

    @TableField(exist = false)
    private InformationEntity informationEntity;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;
}
