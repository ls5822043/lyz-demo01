package com.lyz.shop.controller.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.CommentEntity;
import com.lyz.shop.model.ProductEntity;
import com.lyz.shop.model.SubOrderEntity;
import com.lyz.shop.model.UserEntity;
import com.lyz.shop.model.dto.PutUserInfoByIdDto;
import com.lyz.shop.model.dto.FindProductInfoDto;
import com.lyz.shop.service.CommentService;
import com.lyz.shop.service.ProductService;
import com.lyz.shop.service.SubOrderService;
import com.lyz.shop.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("user")
@Api(tags = "用户功能")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;
    @Autowired
    private SubOrderService subOrderService;
    @Autowired
    private CommentService commentService;

    //用户注册
    @ApiOperation("用户注册")
    @PostMapping("reUser")
    public Base reUser(@RequestBody UserEntity user) {
        //判断需要传进的参数是否为空
        if (StringUtils.isEmpty(user.getName()) || StringUtils.isEmpty(user.getPassword()) || StringUtils.isEmpty(user.getAccountCount()) || StringUtils.isEmpty(user.getSex())
                || StringUtils.isEmpty(user.getNickName()) || StringUtils.isEmpty(user.getPhone()) || StringUtils.isEmpty(user.getBirthdayTime())) {
            return Base.fail(-1, "参数不能为空");
        }
        //判断电话 账号 昵称是否已经被注册过
        LambdaQueryWrapper<UserEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.last("limit 1");
        queryWrapper.eq(UserEntity::getPhone, user.getPhone());
        long count = userService.count(queryWrapper);
        if (count > 0) {
            return Base.fail(-1, "电话已注册");
        }
        LambdaQueryWrapper<UserEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.last("limit 1");
        wrapper.eq(UserEntity::getNickName, user.getNickName());
        long nickCount = userService.count(wrapper);
        if (nickCount > 0) {
            return Base.fail(-1, "昵称已注册");
        }
        LambdaQueryWrapper<UserEntity> account = new LambdaQueryWrapper<>();
        account.last("limit 1");
        account.eq(UserEntity::getAccountCount, user.getAccountCount());
        long accountCount = userService.count(account);
        if (accountCount > 0) {
            return Base.fail(-1, "账号已注册");
        }

        //注册保存
        user.setBalance(BigDecimal.ZERO);
        user.setState(0);//启用
        user.setMember("普通会员");
        user.setIntegral(0L);//积分
        user.setCreatTime(new Date());
        boolean save = userService.save(user);
        if (!save) {
            return Base.fail(-1, "注册失败");
        }
        return Base.data(user);
    }

    //登录 根据账号和密码
    @GetMapping("logUser")
    public Base logUser(String accountCount, String password) {
        if (StringUtils.isEmpty(accountCount) || StringUtils.isEmpty(password)) {
            return Base.fail(-1, "请输入账号和密码");
        }

        //判断账号密码是否正确
        LambdaQueryWrapper<UserEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserEntity::getAccountCount, accountCount);
        wrapper.eq(UserEntity::getPassword, password);
        UserEntity entity = userService.getOne(wrapper);
        if (entity == null) {
            return Base.fail(-1, "账号或密码错误");
        }
        return Base.data(entity);
    }

    //根据id查看用户信息
    @GetMapping("findUserInfoById")
    public Base findUserInfoById(@RequestParam(value = "userId") Long id) {
        UserEntity entity = userService.getById(id);
        return Base.data(entity);
    }

    //修改用户信息
    @PutMapping("putUserInfoById")
    public Base putUserInfoById(@Validated @RequestBody PutUserInfoByIdDto dto) {
        if (StringUtils.isEmpty(dto)) {
            return Base.fail(-1, "id不能为空");
        }
        //查询用户信息是否为空
        UserEntity userEntity = userService.getById(dto.getId());
        if (userEntity == null) {
            return Base.fail(-1, "用户信息不存在");
        }
        //判断昵称是否被占用
        LambdaQueryWrapper<UserEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserEntity::getNickName, dto.getNickName());
        wrapper.ne(UserEntity::getId, dto.getId());
        wrapper.last("limit 1");
        UserEntity entity = userService.getOne(wrapper);
        if (entity != null) {
            return Base.fail(-1, "昵称已存在");
        }
        //判断电话是否存在
        //并且不等于自己的电话
        LambdaQueryWrapper<UserEntity> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserEntity::getPhone, dto.getPhone());
        lambdaQueryWrapper.ne(UserEntity::getId, dto.getId());
        lambdaQueryWrapper.last("limit 1");
        UserEntity phone = userService.getOne(wrapper);
        if (phone != null) {
            return Base.fail(-1, "电话已存在");
        }
        //修改赋值保存
        BeanUtils.copyProperties(dto, userEntity);
        boolean update = userService.updateById(userEntity);
        if (!update) {
            return Base.fail(-1, "修改失败");
        }
        return Base.sure("修改成功");
    }

    //用户注销
    @DeleteMapping("delUserByIdAndPassword")
    public Base delUserByIdAndPassword(Long id, String password) {
        if (StringUtils.isEmpty(id) || StringUtils.isEmpty(password)) {
            return Base.fail(-1, "id和密码不能为空");
        }
        //判断密码是否正确
        LambdaQueryWrapper<UserEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserEntity::getId, id);
        wrapper.eq(UserEntity::getPassword, password);
        boolean remove = userService.remove(wrapper);
        if (!remove) {
            return Base.fail(-1, "id或密码错误");
        }
        return Base.sure("爷销号不玩了");
    }


    //通过type或name或id 查看商品信息
    @GetMapping("userFindProductByTypeOrNameOrId")
    public Base userFindProductByTypeOrNameOrId(ProductEntity dto) {
        if (StringUtils.isEmpty(dto.getType()) && StringUtils.isEmpty(dto.getName())) {
            List<ProductEntity> list = productService.list();
            return Base.data(list);
        }
        LambdaQueryWrapper<ProductEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(ProductEntity::getType, dto.getType()).or().eq(ProductEntity::getName, dto.getName()).or().eq(ProductEntity::getId, dto.getId());
        List<ProductEntity> list = productService.list(wrapper);
        return Base.data(list);
    }

    //用户评价
    @PostMapping("userCommentByUidAndSid")
    public Base userComment(@RequestBody CommentEntity comment) {
        if (comment.getUserId() == null || comment.getSubOrderId() == null || comment.getMark() == null) {
            return Base.fail(-1, "参数不能为空");
        }
        //根据用户id 订单Id判断订单存在
        LambdaQueryWrapper<SubOrderEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SubOrderEntity::getUserId, comment.getUserId());
        wrapper.eq(SubOrderEntity::getId, comment.getSubOrderId());
        SubOrderEntity one = subOrderService.getOne(wrapper);
        if (one == null) {
            return Base.fail(-1, "订单不存在");
        }

        //通过查询sid这条记录判断商品是否已经评价
        LambdaQueryWrapper<CommentEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CommentEntity::getSubOrderId, comment.getSubOrderId());
        long counts = commentService.count(queryWrapper);
        if (counts > 0) {
            return Base.fail(-1, "订单已评价过");
        }

        //新增评价
        comment.setCreatTime(new Date());
        if (comment.getMark() < 1 || comment.getMark() > 5) {
            return Base.fail(-1, "星级只能在1-5之间");
        }
        if (comment.getContent()==null && comment.getMark()>=1 && comment.getMark()<=3){
            comment.setContent("用户觉得不好 并且默默的给了个踩");
        }
        if (comment.getContent()==null && comment.getMark()>=4 && comment.getMark()<=5){
            comment.setContent("用户觉得很好 并且默默的给了个赞");
        }
        boolean save = commentService.save(comment);
        if (!save) {
            return Base.fail(-1, "评价失败");
        }
        return Base.sure("评价成功");
    }
}
