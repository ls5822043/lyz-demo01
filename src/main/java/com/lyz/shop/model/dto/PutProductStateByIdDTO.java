package com.lyz.shop.model.dto;

import lombok.Data;

@Data
public class PutProductStateByIdDTO {

    private Long id;

    private Integer state;
}
