package com.lyz.shop.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.InformationEntity;

public interface InformationService extends IService<InformationEntity> {
}
