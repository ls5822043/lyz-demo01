package com.lyz.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyz.shop.model.RechargeEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RechargeMapper extends BaseMapper<RechargeEntity> {
}
