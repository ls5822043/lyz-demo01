package com.lyz.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.UserMapper;
import com.lyz.shop.model.UserEntity;
import com.lyz.shop.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

//    //用户注册
//    public int reUser(UserEntity userEntity) {
//        userEntity.setBalance(BigDecimal.ZERO);
//        int reUser = baseMapper.insert(userEntity);
//        return reUser;
//}
//
//     // 用户登录
//    public UserEntity logUser(String phone,String password){
//        LambdaQueryWrapper<UserEntity> wrapper = new LambdaQueryWrapper<>();
//        wrapper.eq(UserEntity::getPhone,phone);
//        wrapper.eq(UserEntity::getPassword,password);
//        //用户登录只能由一个账号和密码 所以用one
//        UserEntity logUser = baseMapper.selectOne(wrapper);
//        return logUser;
//    }
//
//    //修改用户信息
//    public int updateUserInfoById(UserEntity userEntity){
//        //判空 查看用户信息看是否为空
//        UserEntity info = baseMapper.selectById(userEntity.getId());
//        if (info==null){
//            return 0;
//        }
//        if (userEntity.getName()!=null){
//            info.setName(userEntity.getName());
//        }
//        if (userEntity.getPassword()!=null){
//            info.setPassword(userEntity.getPassword());
//        }
//        if (userEntity.getPhone()!=null){
//            info.setPhone(userEntity.getPhone());
//        }
//        if (userEntity.getNickName()!=null){
//            info.setNickName(userEntity.getNickName());
//        }
//        if (userEntity.getAddress()!=null){
//            info.setAddress(userEntity.getAddress());
//        }
//        if (userEntity.getIntroduce()!=null){
//            info.setIntroduce(userEntity.getIntroduce());
//        }
//        //保存
//        int save = baseMapper.updateById(info);
//        return save;
//    }
//
//        //注销用户
//        public int logOff(String phone,String password){
//            LambdaQueryWrapper<UserEntity> wrapper = new LambdaQueryWrapper<>();
//            wrapper.eq(UserEntity::getPhone,phone);
//            wrapper.eq(UserEntity::getPassword,password);
//            int logOff = baseMapper.delete(wrapper);
//            return logOff;
//        }
}
