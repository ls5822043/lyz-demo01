package com.lyz.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyz.shop.model.SubOrderEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SubOrderMapper extends BaseMapper<SubOrderEntity> {
}
