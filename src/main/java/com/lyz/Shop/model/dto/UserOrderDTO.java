package com.lyz.shop.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class UserOrderDTO {
    private Long id;

    private String orderNo;

    private Long userId;

    private Long productId;

    private Long cartId;

    private Long businessId;

    private Long totalOrderId;

    private Long number;

    private BigDecimal price;

    private String phone;

    private String name;//收件人名字

    private String address;

    private String message;//用户留言

    private  Integer state;//订单状态 0待支付 1待发货 2已发货 3退款中 4已取消 5已完成

    private Integer source;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;
}
