package com.lyz.shop.controller.business;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.BusinessEntity;
import com.lyz.shop.model.ProductEntity;
import com.lyz.shop.model.SubOrderEntity;
import com.lyz.shop.model.dto.BusinessPutProductDTO;
import com.lyz.shop.model.dto.PutSubOrderByIdDTO;
import com.lyz.shop.service.BusinessService;
import com.lyz.shop.service.ProductService;
import com.lyz.shop.service.SubOrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("business")
public class BusinessController {

    @Autowired
    private BusinessService businessService;
    @Autowired
    private ProductService productService;
    @Autowired
    private SubOrderService subOrderService;

    //商家注册
    @PostMapping("reBusiness")
    public Base reBusiness(@RequestBody BusinessEntity business) {

        if (StringUtils.isEmpty(business.getMerchantAccount()) || StringUtils.isEmpty(business.getName()) ||
                StringUtils.isEmpty(business.getPassword()) || StringUtils.isEmpty(business.getBrand()) || StringUtils.isEmpty(business.getType()) || StringUtils.isEmpty(business.getPhone())) {
            return Base.fail(-1, "参数不能为空");
        }

        //根据账号 编号 电话判断是否已有商家注册
        LambdaQueryWrapper<BusinessEntity> accountWrapper = new LambdaQueryWrapper<>();
        accountWrapper.eq(BusinessEntity::getMerchantAccount, business.getMerchantAccount());
        long accountCount = businessService.count(accountWrapper);
        if (accountCount > 0) {
            return Base.fail(-1, "账号已注册");
        }
        LambdaQueryWrapper<BusinessEntity> phoneWrapper = new LambdaQueryWrapper<>();
        phoneWrapper.eq(BusinessEntity::getPhone, business.getPhone());
        long phoneCount = businessService.count(accountWrapper);
        if (phoneCount > 0) {
            return Base.fail(-1, "电话已注册");
        }

        //注册保存
        business.setState(0);
        business.setCreatTime(new Date());
        boolean save = businessService.save(business);
        if (!save) {
            return Base.fail(-1, "注册失败");
        }
        return Base.sure("注册成功");
    }

    //商家账号密码登录
    @GetMapping("businessLogin")
    public Base businessLogin(String merchantAccount, String password) {

        if (StringUtils.isEmpty(merchantAccount) || StringUtils.isEmpty(password)) {
            return Base.fail(-1, "账号密码不能为空");
        }
        LambdaQueryWrapper<BusinessEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BusinessEntity::getMerchantAccount, merchantAccount);
        wrapper.eq(BusinessEntity::getPassword, password);
        wrapper.eq(BusinessEntity::getState,0);
        wrapper.last(" limit 1");
        BusinessEntity serviceOne = businessService.getOne(wrapper);
        if (serviceOne == null) {
            return Base.fail(-1, "账号或密码错误");
        }
        return Base.data(serviceOne);
    }

    //修改商家信息
    @PutMapping("putBusinessById")
    public Base putBusinessById(@RequestBody BusinessEntity business) {

        if (StringUtils.isEmpty(business.getId())) {
            return Base.fail(-1, "id不能为空");
        }
        //判断商家信息是否为空
        BusinessEntity entity = businessService.getById(business.getId());
        if (entity == null) {
            return Base.fail(-1, "商家不存在");
        }

        //判断要修改的电话是否被占用
        //并且有一个电话是自己的 所以要判断那个电话是否是自己的
        //通过id不等于来确定不是自己的电话
        LambdaQueryWrapper<BusinessEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BusinessEntity::getPhone, business.getPhone());
        wrapper.ne(BusinessEntity::getId, business.getId());
        long count = businessService.count(wrapper);
        if (count > 0) {
            return Base.fail(-1, "电话已占用");
        }

        //修改 保存
        //电话 名字 密码 简介 分类 状态
        business.setMerchantAccount(null);
        business.setBrand(null);
        business.setUpdateTime(new Date());
        boolean update = businessService.updateById(business);
        if (!update) {
            return Base.fail(-1, "修改失败");
        }
        return Base.sure("修改成功");
    }

    //注销商家
    @DeleteMapping("delBusinessByAccountAndPassword")
    public Base delBusinessByAccountAndPassword(String merchantAccount, String password) {
        if (StringUtils.isEmpty(merchantAccount) || StringUtils.isEmpty(password)) {
            return Base.fail(-1, "账号和密码不能为空");
        }
        //判断账号密码是否错误
        LambdaQueryWrapper<BusinessEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BusinessEntity::getMerchantAccount, merchantAccount);
        wrapper.eq(BusinessEntity::getPassword, password);
        boolean remove = businessService.remove(wrapper);
        if (!remove) {
            return Base.fail(-1, "账号或密码错误");
        }
        return Base.sure("注销成功");
    }

    //展示商品列表
    //通过type或 name 查看商品信息
    @GetMapping("businessFindProductByTypeOrNameOrId")
    public Base userFindProductByTypeOrNameOrId(String name,String type){
        if (StringUtils.isEmpty(name)&&StringUtils.isEmpty(type)){
            List<ProductEntity> list = productService.list();
            return Base.data(list);
        }
        LambdaQueryWrapper<ProductEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(ProductEntity::getType,type).or().eq(ProductEntity::getName,name).or();
        List<ProductEntity> list = productService.list(wrapper);
        return Base.data(list);
    }

    //商家修改商品信息
    @PutMapping("businessPutProduct")
    public Base businessPutProduct(@RequestBody BusinessPutProductDTO dto){
        if (dto.getId()==null){
            return Base.fail(-1,"id不能为空");
        }
        //查找商品信息 看是否存在
        ProductEntity productEntity = productService.getById(dto.getId());
        if (productEntity==null){
            return Base.fail(-1,"商品信息不存在");
        }
        //如果要传入数量 则将数量和库存叠加
        //增加库存
        if (productEntity.getNumber()!=0){
            Long stock = productEntity.getStock();
            Long number = productEntity.getNumber();
            productEntity.setStock(stock+number);
            productService.updateById(productEntity);
        }
        //商品信息存在 则进行赋值修改
        dto.setUpdateTime(new Date());
        BeanUtils.copyProperties(dto,productEntity);

        boolean update = productService.updateById(productEntity);
        if (!update){
            return Base.fail(-1,"修改失败");
        }
        return Base.sure("修改成功");
    }

    //商家删除商品
    @DeleteMapping("businessDelProductByPidAndBid")
    public Base businessDelProductByPidAndBid(Long businessId,Long productId){
        if (StringUtils.isEmpty(businessId)||StringUtils.isEmpty(productId)){
            return Base.fail(-1,"bid和pid不能为空");
        }
        //通过商家id和商品id
        LambdaQueryWrapper<ProductEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ProductEntity::getId,productId);
        wrapper.eq(ProductEntity::getBusinessId,businessId);
        boolean remove = productService.remove(wrapper);
        if (!remove){
            return Base.fail(-1,"商品已删除或不存在");
        }
        return Base.sure("删除成功");
    }

    //商家通过电话或订单号查询用户
    @GetMapping("findUserByPhoneOrOrderNo")
    public Base findUserByPhoneOrOrderNo(String phone,String orderNo){
        if (StringUtils.isEmpty(phone) && StringUtils.isEmpty(orderNo)){
            return Base.fail(-1,"电话或订单号不能为空");
        }
        LambdaQueryWrapper<SubOrderEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SubOrderEntity::getOrderNo,orderNo)
                .or().eq(SubOrderEntity::getPhone,phone);
        SubOrderEntity one = subOrderService.getOne(wrapper);
        if (one==null){
            return Base.fail(-1,"用户不存在");
        }
        return Base.data(one);
    }

    //商家查看店铺下所有订单
    @GetMapping("businessFindOrderByBusinessId")
    public Base businessFindOrderByBusinessId(@RequestParam(value = "businessId") Long id){
        if (StringUtils.isEmpty(id)){
            return Base.fail(-1,"id不能为空");
        }
        LambdaQueryWrapper<SubOrderEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SubOrderEntity::getBusinessId,id);
        List<Map<String, Object>> maps = subOrderService.listMaps(wrapper);
        return Base.data(maps);
    }

    //修改订单(添加物流单号和快递)
    //订单状态为1 可以加入快递和快递单号 也可以帮客户修改地址信息等
    //订单状态 0待支付 1待发货 2已发货 3退款中 4已取消 5已完成
    @PutMapping("putSubOrderById")
    public Base putSubOrderById(@RequestBody PutSubOrderByIdDTO dto){
        //判断传入的订单Id 订单状态 source状态
        if (dto.getId()==null||dto.getState()==null||dto.getSource()==null){
            return Base.fail(-1,"传入的参数不能为空");
        }
        //根据source的状态判断修改地址还是传入快递
        if (dto.getSource()==0 &&dto.getState()==1){
            //修改地址信息
            //判断订单信息是否为空
            SubOrderEntity subOrderEntity = subOrderService.getById(dto.getId());
            if (subOrderEntity==null){
                return Base.fail(-1,"订单信息为空");
            }
            dto.setExpress(null);
            dto.setExpressNo(null);
            BeanUtils.copyProperties(dto,subOrderEntity);
            boolean update = subOrderService.updateById(subOrderEntity);
            if (!update){
                return Base.fail(-1,"地址修改失败");
            }
            return Base.sure("地址修改成功");
        }
        if (dto.getSource()==1 && dto.getState()==1){
            //传入快递和快递单号
            SubOrderEntity subOrderEntity = subOrderService.getById(dto.getId());
            if (subOrderEntity==null){
                return Base.fail(-1,"订单信息为空");
            }
            dto.setPhone(null);
            dto.setName(null);
            dto.setAddress(null);
            BeanUtils.copyProperties(dto,subOrderEntity);
            boolean update = subOrderService.updateById(subOrderEntity);
            if (!update){
                return Base.fail(-1,"快递修改失败");
            }
            return Base.sure("快递修改成功");
        }
        return Base.sure("操作异常");
    }

//    //确认收货
//    @GetMapping("finishOrder/{id}")
//    public Base finishOrder(@PathVariable("id")Long id) {
//
//    }

}
