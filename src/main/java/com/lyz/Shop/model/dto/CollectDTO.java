package com.lyz.shop.model.dto;

import lombok.Data;

@Data
public class CollectDTO {
    private Long userId;

    private Long targetId;

    private Integer type;

    private Integer source;
}
