package com.lyz.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.ProductMapper;
import com.lyz.shop.model.ProductEntity;
import com.lyz.shop.service.ProductService;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, ProductEntity> implements ProductService {

//    //新增商品
//    public int addProduct(ProductEntity productEntity){
//        //前端已经传进参数
//        //将时间初始化
//        productEntity.setCreatTime(new Date());
//        //然后保存
//        int insert = baseMapper.insert(productEntity);
//        return insert;
//    }
//
//    //查看商品
//    public ProductEntity findProductInfoById(Long id){
//        ProductEntity productEntity = baseMapper.selectById(id);
//        return productEntity;
//    }
//    //修改商品信息
//    @Override
//    public int updateProductInfo(ProductEntity productEntity){
//        //判空 查看商品信息是否为空
//        ProductEntity info = baseMapper.selectById(productEntity.getId());
//        if (info==null){
//            return 0;
//        }
//        if (productEntity.getName()!=null){
//            info.setName(productEntity.getName());
//        }
//        if (productEntity.getPrice()!=null){
//            info.setPrice(productEntity.getPrice());
//        }
//        if (productEntity.getColor()!=null){
//            info.setColor(productEntity.getColor());
//        }
//        if (productEntity.getFreight()!=null){
//            info.setFreight(productEntity.getFreight());
//        }
//        if (productEntity.getType()!=null){
//            info.setType(productEntity.getType());
//        }
//        if (productEntity.getWeight()!=null){
//            info.setWeight(productEntity.getWeight());
//        }
//        if (productEntity.getNumber()!=null){
//            info.setNumber(productEntity.getNumber());
//        }
//        if (productEntity.getStock()!=null){
//            info.setStock(productEntity.getStock());
//        }
//        if (productEntity.getBrand()!=null){
//            info.setBrand(productEntity.getBrand());
//        }
//        info.setUpdateTime(new Date());
//        //保存
//        int save = baseMapper.updateById(info);
//        return save;
//    }
//    //删除商品
//    public int delProductById(Long id){
//        int delete = baseMapper.deleteById(id);
//        return delete;
//    }
}
