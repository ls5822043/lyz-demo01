package com.lyz.shop.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class BusinessPutProductDTO {

    private Long id;

    private String name;

    private BigDecimal price;

    private String color;

    private String freight;//运费

    private String type;//种类

    private String weight;//重量;

    private Long number;//数量

    private Long stock;//库存

    private Integer state;//状态 0上架  1下架

    private String  picture;//照片

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date updateTime;


}
