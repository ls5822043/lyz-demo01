package com.lyz.shop.model.dto;

import lombok.Data;

@Data

public class PutUserInfoByIdDto {

    private Long id;

    private String name;

    private String nickName;

    private String password;

    private String phone;

    private String address;//地址

     private String introduce;//简介

}
