package com.lyz.shop.controller.function;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.CartEntity;
import com.lyz.shop.model.ProductEntity;
import com.lyz.shop.model.dto.CartDto;
import com.lyz.shop.model.dto.DelCartDTO;
import com.lyz.shop.service.CartService;
import com.lyz.shop.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("cart")
public class CartController {

    @Autowired
    private CartService cartServer;
    @Autowired
    private ProductService productServer;


    //添加购物车
    @PostMapping("cart")
    public Base cart(@RequestBody CartDto dto){
        if (dto.getSource()==null||dto.getUserId()==null||dto.getProductId()==null){
            return Base.fail(-1,"source不能为空");
        }
        if (dto.getSource()==0){//新增
            //查找购物车 看是否存在同款商品
            LambdaQueryWrapper<CartEntity> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(CartEntity::getUserId,dto.getUserId());
            wrapper.eq(CartEntity::getProductId,dto.getProductId());
            wrapper.last(" limit 1");
            CartEntity cartServerOne = cartServer.getOne(wrapper);
            //如果购物车里面是有数量的 则数量+1
            if (cartServerOne!=null){
                Long number = cartServerOne.getNumber();
                cartServerOne.setNumber(number+1);
                //更新购物车
                cartServer.updateById(cartServerOne);
                if (cartServerOne.getNumber()>99){
                    return Base.fail(-1,"购物车已到上限");
                }
                return Base.sure("购物车更新成功");
            }
            //如果购物车不存在
            //新增购物车
            //每次新增默认为1
            CartEntity cartEntity = new CartEntity();
            BeanUtils.copyProperties(dto,cartEntity);
            cartEntity.setCreatTime(new Date());
            cartEntity.setNumber(1l);
            boolean save = cartServer.save(cartEntity);
            if (!save){
                return Base.fail(-1,"购物车添加失败");
            }
            return Base.sure("购物车添加成功");

        }else {//删除购物车数量
            //查询购物车 拿到购物车数量
            LambdaQueryWrapper<CartEntity> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(CartEntity::getUserId,dto.getUserId());
            wrapper.eq(CartEntity::getProductId,dto.getProductId());
            wrapper.last(" limit 1");
            CartEntity entity = cartServer.getOne(wrapper);
            if (entity.getNumber()!=0){
                //数量-1
                Long number = entity.getNumber();
                entity.setNumber(number-1);
                //更新购物车数量
                boolean update = cartServer.updateById(entity);
                }
            if (entity.getNumber()==0){
                LambdaQueryWrapper<CartEntity> queryWrapper = new LambdaQueryWrapper<>();
                queryWrapper.eq(CartEntity::getUserId,dto.getUserId());
                queryWrapper.eq(CartEntity::getProductId,dto.getProductId());
                boolean remove = cartServer.remove(queryWrapper);
                if (!remove){
                    return Base.fail(-1,"删除失败");
                }
                return Base.sure("删除成功");
            }
        }
        return Base.fail(-1,"操作成功");
    }

   //展示购物车
    @GetMapping("findCartByUid/{userId}")
    public Base findCartByUid(@PathVariable("userId") Long userId){
        LambdaQueryWrapper<CartEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CartEntity::getUserId,userId);
        List<CartEntity> list = cartServer.list(wrapper);

        //遍历 拿到商品信息
        list.stream().forEach(item->{
            ProductEntity entity = productServer.getById(item.getProductId());
            item.setProductEntity(entity);
        });
        return Base.data(list);
    }

   //修改购物车数量
    @PutMapping("putCartById")
    public Base putCartById(@RequestBody CartEntity cart){
        if (cart.getId()==null||cart.getNumber()==null){
            return Base.fail(-1,"参数不能为空");
        }

        //传入的数量不能为0
        cart.setUserId(null);
        cart.setNumber(cart.getNumber());
        if (cart.getNumber()<=0){
            return Base.fail(-1,"商品不能在修改了");
        }
        boolean update = cartServer.updateById(cart);
        if (!update){
            return Base.fail(-1,"修改失败");
        }
        return Base.sure("修改成功");
    }

    //删除单个购物车或清空购物车
    @DeleteMapping("delCart")
    public Base delCart(@RequestBody DelCartDTO dto){
        if (dto.getSource()==null){
            return Base.fail(-1,"source不能为空");
        }

        //删除单个购物车
        if (dto.getSource()==0){
            //根据id和pid
            LambdaQueryWrapper<CartEntity> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(CartEntity::getId,dto.getId());
            wrapper.eq(CartEntity::getProductId,dto.getProductId());
            boolean remove = cartServer.remove(wrapper);
            if (!remove){
                return Base.fail(-1,"单个商品删除失败");
            }
            return Base.sure("单个商品删除成功");
        }
        if (dto.getSource()==1){
            //根据uid清空购物车
            LambdaQueryWrapper<CartEntity> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(CartEntity::getUserId,dto.getUserId());
            boolean remove = cartServer.remove(wrapper);
            if (!remove){
                return Base.fail(-1,"一键删除购物车失败");
            }
            return Base.sure("一键删除购物车成功");
        }
        return Base.fail(-1,"操作失败");
    }
}
