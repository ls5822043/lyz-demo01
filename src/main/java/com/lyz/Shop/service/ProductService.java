package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.ProductEntity;

public interface ProductService extends IService<ProductEntity> {

}
