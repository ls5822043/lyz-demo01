package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.BusinessEntity;

public interface BusinessService extends IService<BusinessEntity> {

}
