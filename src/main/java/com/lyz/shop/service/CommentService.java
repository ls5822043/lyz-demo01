package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.CommentEntity;

public interface CommentService extends IService<CommentEntity> {
}
