package com.lyz.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyz.shop.model.ProductEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface ProductMapper extends BaseMapper<ProductEntity> {


}
