package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.CapitalEntity;

public interface CapitalService extends IService<CapitalEntity> {
}
