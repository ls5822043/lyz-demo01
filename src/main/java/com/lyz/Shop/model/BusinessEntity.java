package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("business")
public class BusinessEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private String merchantAccount;//商家账号

    private String  password;

    private String phone;

    private String brand;//品牌

    private String introduce;//描述

    private String type;//类型

    private Integer state;//0启用 -1禁用 -2黑名单

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;//注册时间

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date updateTime;//修改时间

}
