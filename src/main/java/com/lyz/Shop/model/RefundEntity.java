package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("refund")
public class RefundEntity {

    private Long id;

    private Long userId;

    private Long subOrderId;

    private Integer refund;//退款记录 0 退款中 1已退款 2处理中

    private BigDecimal balance;//退款金额

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;
}
