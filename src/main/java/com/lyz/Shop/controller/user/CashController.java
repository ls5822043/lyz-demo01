package com.lyz.shop.controller.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.CapitalEntity;
import com.lyz.shop.model.CartEntity;
import com.lyz.shop.model.CashEntity;
import com.lyz.shop.model.UserEntity;
import com.lyz.shop.service.CapitalService;
import com.lyz.shop.service.CashService;
import com.lyz.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("cash")
public class CashController {
    //提现
    @Autowired
    private CashService cashService;
    @Autowired
    private UserService userService;
    @Autowired
    private CapitalService capitalService;

    //用户提现
    @PostMapping("userCash")
    public Base userCash(@RequestBody CashEntity cashEntity){
        if (cashEntity.getUserId()==null||cashEntity.getCashBalance()==null){
            return Base.fail(-1,"参数不能为空");
        }
        //判断用户和金额是否<=提现金额
        UserEntity userId = userService.getById(cashEntity.getUserId());
        if (userId==null){
            return Base.fail(-1,"用户不存在");
        }
       if (userId.getBalance().compareTo(BigDecimal.ZERO)==0 || userId.getBalance().compareTo(cashEntity.getCashBalance())<=-1){
           return Base.fail(-1,"余额不足");
       }else {
           //余额充足则会从余额里面-去提现金额
           Long integral = userId.getIntegral();
           BigDecimal cashBa = cashEntity.getCashBalance();
           BigDecimal bigDecimal = new BigDecimal(String.valueOf(cashBa));
           Long bigBalance = bigDecimal.longValue();

           BigDecimal balance = userId.getBalance();

           BigDecimal cashBalance = cashEntity.getCashBalance();
           userId.setBalance(balance.subtract(cashBalance));
           userId.setIntegral(integral-bigBalance);
           boolean updateById = userService.updateById(userId);
       }
       //提现后 对应的积分会减少
        //查询用户积分
        cashEntity.setCreatTime(new Date());
       cashEntity.setReceiptTime(new Date());
        boolean save = cashService.save(cashEntity);
        if (!save){
            return Base.fail(-1,"提现失败");
        }
        //关联资金明细
        BigDecimal balance = userId.getBalance();
        BigDecimal cashBalance = cashEntity.getCashBalance();
        CapitalEntity capitalEntity = new CapitalEntity();
        capitalEntity.setTargetId(cashEntity.getId());
        capitalEntity.setUserId(cashEntity.getUserId());
        capitalEntity.setType(2);
        capitalEntity.setTips("提现");
        capitalEntity.setFundDetails(balance.add(cashBalance));
        capitalEntity.setBalanceDetails(balance);
        capitalEntity.setCreatTime(new Date());
        capitalService.save(capitalEntity);
        return Base.sure("提现成功");
    }

    //查看提现记录
    //UID
    @GetMapping("findCashByUid")
    public Base findCashByUid(@RequestParam(value = "userId") Long userId){
        LambdaQueryWrapper<CashEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CashEntity::getUserId,userId);
        List<CashEntity> list = cashService.list(wrapper);
        return Base.data(list);
    }

    //删除提现记录
    @DeleteMapping("delCashById")
    public Base delCashById(Long id){
        boolean remove = cashService.removeById(id);
        if (!remove){
            return Base.fail(-1,"删除提现记录失败");
        }
        return Base.sure("删除提现记录成功");
    }
}
