package com.lyz.shop.controller.function;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.BusinessEntity;
import com.lyz.shop.model.CollectEntity;
import com.lyz.shop.model.ProductEntity;
import com.lyz.shop.model.dto.PutProductStateByIdDTO;
import com.lyz.shop.service.BusinessService;
import com.lyz.shop.service.CollectService;
import com.lyz.shop.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("product")
public class ProductController {

    @Autowired
    private ProductService productServer;
    @Autowired
    private BusinessService businessService;
    @Autowired
    private CollectService collectService;

    //新增商品
    @PostMapping("addProduct")
    public Base addProduct(@RequestBody ProductEntity product) {
        if (StringUtils.isEmpty(product.getProductNo())||StringUtils.isEmpty(product.getName())||StringUtils.isEmpty(product.getColor())||
        StringUtils.isEmpty(product.getPrice())||StringUtils.isEmpty(product.getFreight())||StringUtils.isEmpty(product.getWeight())||
        StringUtils.isEmpty(product.getType())||StringUtils.isEmpty(product.getNumber())||StringUtils.isEmpty(product.getStock())||
        StringUtils.isEmpty(product.getBrand())||StringUtils.isEmpty(product.getBusinessId())){
            return Base.fail(1,"商品信息不能为空");
        }

        //通过no 看有没有已经存在的
        LambdaQueryWrapper<ProductEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ProductEntity::getProductNo,product.getProductNo());
        long count = productServer.count(wrapper);
        if (count>0){
            return Base.fail(1,"编号已存在");
        }
        //通过id查找商家是否存在
        BusinessEntity byId = businessService.getById(product.getBusinessId());
        if (byId==null){
            return Base.fail(-1,"商家不存在");
        }

        //新增商品
        product.setSale(0L);
        product.setState(0);
        product.setCreatTime(new Date());
        boolean save = productServer.save(product);
        if (!save){
            return Base.fail(1,"新增失败");
        }
        return Base.sure("新增成功");
    }

    //通过id查看商品信息
    @GetMapping("findProductInfoById")
    public Base findProductInfoById(Long id,Long userId) {
        ProductEntity find = productServer.getById(id);
        if (find==null){
            return Base.fail(1,"商品不存在");
        }
        //判断是否收藏了此件商品
        //uid是判断是否登录
       if(userId!=null){
           //不等于空 则判断商品是否收藏
           Long count = collectService.countByUserIdAndTargetIdAndType(userId, id, 0);
           //>0 则是收藏 isSave会变成true(商品被收藏状态)
           find.setIsSave(count>0);
       }
       //如果没有登录 也可以通过id查看商品信息
        BusinessEntity businessEntity = businessService.getById(find.getBusinessId());
        if(businessEntity!=null){
            find.setBusinessName(businessEntity.getName());
        }
        return Base.data(find);
    }

    //修改商品信息
    @PutMapping("updateProductInfoById")
    public Base updateProductInfoById(@RequestBody ProductEntity productEntity){
        if(StringUtils.isEmpty(productEntity.getId())){
            return Base.fail(1,"商品id不能为空");
        }
        //判断商品信息是否为空
        ProductEntity byId = productServer.getById(productEntity.getId());
        if (byId==null){
            return Base.fail(1,"商品不存在");
        }
        //判断商品名字是否存在
        LambdaQueryWrapper<ProductEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ProductEntity::getName,productEntity.getName());
        ProductEntity one = productServer.getOne(wrapper);

        if (one!=null){
            //如果商品名已经存在 并且有传入数量库存等 直接将其进行相加
            Long number = one.getNumber();
            Long stock = one.getStock();
            one.setStock(number+stock);

            one.setUpdateTime(new Date());
            productServer.updateById(one);
        }

        //保存修改
        productEntity.setUpdateTime(new Date());
        productEntity.setProductNo(null);
        productEntity.setSale(null);
        productEntity.setBusinessId(null);
        Boolean update = productServer.updateById(productEntity);
        if (!update){
            return Base.fail(1,"修改失败");
        }
        return Base.sure("修改成功");
    }

    //通过id修改商品状态
    @PutMapping("putProductStateById")
    public Base putProductStateById(@RequestBody PutProductStateByIdDTO dto){
        if (dto.getId()==null||dto.getState()==null){
            return Base.fail(-1,"请输入参数");
        }
        ProductEntity productEntity = productServer.getById(dto.getId());
        if (productEntity==null){
            return Base.fail(-1,"商品不存在");
        }
        BeanUtils.copyProperties(dto,productEntity);
        boolean updateById = productServer.updateById(productEntity);
        if (!updateById){
            return Base.fail(-1,"修改失败");
        }
        return Base.sure("商品下架成功");
    }

    //展示商品列表
    //通过id或名字或分类搜索商品
    @GetMapping("findProductByTypeOrName")
    public Base findProductByTypeOrName(ProductEntity productEntity){
        //如果没有输入类型或名字 则根据id查看商品信息
        if (productEntity.getType()==null&&productEntity.getName()==null){
            List<ProductEntity> list = productServer.list();
            return Base.data(list);
        }
        LambdaQueryWrapper<ProductEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(ProductEntity::getType,productEntity.getType()).or().like(ProductEntity::getName,productEntity.getName());
        List<ProductEntity> list = productServer.list(wrapper);
        return Base.data(list);
    }
}
