package com.lyz.shop.model.dto;

import lombok.Data;

@Data
public class PutSubOrderByIdDTO {

    private Long id;

    private  Integer state;//订单状态 0待支付 1待发货 2已发货 3退款中 4已取消 5已完成

    private String express;//快递

    private String expressNo;//快递单号

    private String phone;

    private String name;//收件人名字

    private String address;

    private Integer source;
}
