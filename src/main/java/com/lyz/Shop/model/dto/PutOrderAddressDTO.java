package com.lyz.shop.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class PutOrderAddressDTO {

    private Long id;

    private String phone;

    private String name;//收件人名字

    private String address;


    private  Integer state;//订单状态 0待支付 1待发货 2已发货 3退款中 4已取消 5已完成


    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date updateTime;


}
