package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName("product")
@Data
public class ProductEntity {
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long businessId;

    private String productNo;

    private String name;

    private BigDecimal price;

    private String color;

    private String freight;//运费

    private String type;//种类

    private String weight;//重量;

    private Long number;//数量

    private Long stock;//库存

    private Long sale;//销量

    private String brand;//品牌

    private Integer state;//状态 0上架  1下架

    private String  picture;//照片

    @TableField(exist = false)//不查数据库
    private Boolean isSave=false;

    @TableField(exist = false)
    private String businessName;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date updateTime;
}
