package com.lyz.shop.controller.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.AddressEntity;
import com.lyz.shop.model.UserEntity;
import com.lyz.shop.service.AddressService;
import com.lyz.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("address")
public class AddressController {

    @Autowired
    private AddressService addressService;
    @Autowired
    private UserService userService;

    //新增地址
    @PostMapping("address")
    public Base address(@RequestBody AddressEntity address){
        if (address.getUserId()==null|| StringUtils.isEmpty(address.getProvince())||StringUtils.isEmpty(address.getCity())
         ||StringUtils.isEmpty(address.getArea())||StringUtils.isEmpty(address.getAddressDetailed())||StringUtils.isEmpty(address.getTownship())||
        StringUtils.isEmpty(address.getPhone())||StringUtils.isEmpty(address.getName())){
            return Base.fail(-1,"参数不能为空");
        }
        //进行地址赋值、保存
        //判断用户是否为空、登录
        UserEntity service = userService.getById(address.getUserId());
        if (service==null){
            return Base.fail(-1,"请先登录或注册账号");
        }
        address.setCreatTime(new Date());
        boolean save = addressService.save(address);
        if (!save){
            return Base.fail(-1,"保存失败");
        }
        return Base.sure("保存成功");
    }

    //展示地址列表
    @GetMapping("findAddress")
    public  Base findAddress(@RequestParam(value = "userId") Long userId) {
        LambdaQueryWrapper<AddressEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AddressEntity::getUserId,userId);
        List<AddressEntity> list = addressService.list(wrapper);
        return Base.data(list);
    }

    //修改地址
    @PutMapping("putAddressById")
    public Base putAddressById(@RequestBody AddressEntity address){

        boolean updateById = addressService.updateById(address);
        if (!updateById){
            return Base.fail(-1,"修改失败 请传入id");
        }
        return Base.sure("修改成功");
    }

    //删除地址
    @DeleteMapping("delAddressById")
    public Base delAddressById(Long id){
        boolean remove = addressService.removeById(id);
        if (!remove){
            return Base.fail(-1,"删除失败");
        }
        return Base.sure("删除成功");
    }
}
