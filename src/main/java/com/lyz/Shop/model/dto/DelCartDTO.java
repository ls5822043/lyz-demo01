package com.lyz.shop.model.dto;

import lombok.Data;

@Data
public class DelCartDTO {

    private Long id;

    private Long productId;

    private Long userId;

    private Integer source;
}
