package com.lyz.shop.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("Admin")
public class AdminEntity {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String accountNumber;

    private String nickName;

    private String name;

    private String password;

    private String phone;

    private Integer sex;//0男 1女

    private Integer state;//0启用 -1禁用

}
