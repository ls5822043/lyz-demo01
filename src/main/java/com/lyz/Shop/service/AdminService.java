package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.AdminEntity;

public interface AdminService extends IService<AdminEntity>{

}
