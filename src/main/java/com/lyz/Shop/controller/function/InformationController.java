package com.lyz.shop.controller.function;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.BusinessEntity;
import com.lyz.shop.model.InformationEntity;
import com.lyz.shop.service.BusinessService;
import com.lyz.shop.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("information")
public class InformationController {

    @Autowired
    private InformationService informationService;
    @Autowired
    private BusinessService businessService;

    //新增资讯
    @PostMapping("addInformation")
    public Base addInformation(@RequestBody InformationEntity information){
        if (StringUtils.isEmpty(information.getAuthor())||StringUtils.isEmpty(information.getHeadline())||StringUtils.isEmpty(information.getContent())
                ||StringUtils.isEmpty(information.getBusinessId())||StringUtils.isEmpty(information.getPicture())){
            return Base.fail(-1,"资讯内容不能为空");
        }
        //查询商家是否存在
        BusinessEntity byId = businessService.getById(information.getBusinessId());
        if (byId==null){
            return Base.fail(-1,"商家不存在");
        }

        //查询商家和同样的标题是否发布过
        LambdaQueryWrapper<InformationEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(InformationEntity::getHeadline,information.getHeadline());
        queryWrapper.eq(InformationEntity::getBusinessId,information.getBusinessId());
        long l = informationService.count(queryWrapper);
        if (l>0){
            return Base.fail(-1,"资讯标题已经发表过");
        }
        //进行资讯发布
        information.setCreatTime(new Date());
        boolean save = informationService.save(information);
        if (!save){
            return Base.fail(-1,"发布失败");
        }
        return Base.sure("发布成功");
    }

    //展示资讯
    //通过id
    //通过作者或者标题查询资讯
    @GetMapping("findInformationByAuthorOrHeadline")
    public Base findInformationByAuthorOrHeadline(String author,String headline,Long id){
        if (StringUtils.isEmpty(author)&&StringUtils.isEmpty(headline)){
            List<InformationEntity> list = informationService.list();
            return Base.data(list);
        }
        LambdaQueryWrapper<InformationEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(InformationEntity::getAuthor,author).or().like(InformationEntity::getHeadline,headline);
        List<Map<String, Object>> maps = informationService.listMaps(wrapper);
        return Base.data(maps);
    }


    //修改资讯
    @PutMapping("putInformationById")
    public Base putInformationById(@RequestBody InformationEntity information) {
        if (StringUtils.isEmpty(information.getId())) {
            return Base.fail(-1, "id不能为空");
        }
        //查询标题是否发布过
        //并且不等于自己的id标题
        LambdaQueryWrapper<InformationEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(InformationEntity::getHeadline, information.getHeadline());
        queryWrapper.ne(InformationEntity::getId,information.getId());
        long l = informationService.count(queryWrapper);
        if (l > 0) {
            return Base.fail(-1, "资讯标题已存在");
        }
        information.setUpdateTime(new Date());
        information.setAuthor(null);
        information.setBusinessId(null);
        boolean b = informationService.updateById(information);
        if (!b) {
            return Base.fail(-1, "修改失败");
        }
        return Base.sure("修改成功");
    }

    //删除资讯
    @DeleteMapping("delInformationById")
    public Base delInformationById(Long id){
        if (StringUtils.isEmpty(id)){
            return Base.fail(-1,"id不能为空");
        }
        boolean remove = informationService.removeById(id);
        if (!remove){
            return Base.fail(-1,"资讯已删除或不存在");
        }
        return Base.sure("删除成功");
    }
}
