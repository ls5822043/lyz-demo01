package com.lyz.shop.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class FindProductInfoDto {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long businessId;

    private String productNo;

    private String name;

    private BigDecimal price;

    private String color;

    private String freight;//运费

    private String type;//种类

    private String weight;//重量;

    private Long number;//数量

    private Long sale;//销量

    private String brand;//品牌

    private Integer state;//状态 0上架  -1下架
}
