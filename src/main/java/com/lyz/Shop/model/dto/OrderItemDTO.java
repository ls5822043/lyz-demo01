package com.lyz.shop.model.dto;

import lombok.Data;
@Data
public  class OrderItemDTO{

    private Long productId;

    private Integer number;
}
