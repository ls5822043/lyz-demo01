package com.lyz.shop.model.dto;

import lombok.Data;

@Data
public class DelRechargeDTO {

    private Long id;

    private Long userId;

    private Integer source;
}
