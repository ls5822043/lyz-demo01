package com.lyz.shop.controller.function;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lyz.shop.base.Base;
import com.lyz.shop.model.CollectEntity;
import com.lyz.shop.model.dto.CollectDTO;
import com.lyz.shop.service.CollectService;
import com.lyz.shop.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("collect")
public class CollectController {

    @Autowired
    private CollectService collectService;
    @Autowired
    private ProductService productServer;

    //新增收藏\删除收藏
    @PostMapping("collect")
    public Base collect(@RequestBody CollectDTO dto){
        if (dto.getUserId()==null||dto.getTargetId()==null||dto.getType()==null||dto.getSource()==null){
            return Base.fail(-1,"参数不能为空");
        }
        if(dto.getSource()==0){
            LambdaQueryWrapper<CollectEntity> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(CollectEntity::getUserId,dto.getUserId());
            wrapper.eq(CollectEntity::getType,dto.getType());
            wrapper.eq(CollectEntity::getTargetId,dto.getTargetId());
            long count = collectService.count(wrapper);
            if(count>0){
                return Base.fail(-1,"勿重复收藏");
            }
            CollectEntity collectEntity=new CollectEntity();
            BeanUtils.copyProperties(dto,collectEntity);
            collectEntity.setCreatTime(new Date());
            boolean save = collectService.save(collectEntity);
            if (!save){
                return Base.fail(-1,"收藏失败");
            }
            return Base.sure("收藏成功");
        }else{
            boolean remove = collectService.remove(new LambdaQueryWrapper<CollectEntity>()
                    .eq(CollectEntity::getUserId, dto.getUserId())
                    .eq(CollectEntity::getTargetId, dto.getTargetId())
                    .eq(CollectEntity::getType, dto.getType())
            );
            return Base.sure("取消成功");
        }
    }

    //查看收藏
    @GetMapping("findCollectById")
    public Base findCollectById(@RequestParam(value = "id") Long id){
        CollectEntity byId = collectService.getById(id);
        if (byId==null){
            return Base.fail(-1,"收藏不存在");
        }
        return Base.data(byId);
    }

    //通过分组查看收藏
    @GetMapping("findCollectByType")
    public Base findCollectByType(@RequestParam("type") Integer type){
        LambdaQueryWrapper<CollectEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CollectEntity::getType,type);
        List<CollectEntity> list = collectService.list(wrapper);
        return Base.data(list);
    }

}

