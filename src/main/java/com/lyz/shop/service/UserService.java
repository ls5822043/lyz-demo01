package com.lyz.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyz.shop.model.UserEntity;

public interface UserService extends IService<UserEntity> {

}
