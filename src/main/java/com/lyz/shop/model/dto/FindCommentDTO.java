package com.lyz.shop.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class FindCommentDTO {

    private Long id;

    private Long userId;

    private Long productId;

    private Long subOrderId;

    private Integer mark;//星级

    private Integer source;

    private String content;//评价内容

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date creatTime;
}
