package com.lyz.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.TotalOrderMapper;
import com.lyz.shop.model.TotalOrderEntity;
import com.lyz.shop.service.TotalOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TotalOrderServiceImpl extends ServiceImpl<TotalOrderMapper, TotalOrderEntity> implements TotalOrderService {

}
