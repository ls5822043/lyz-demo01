package com.lyz.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyz.shop.mapper.CapitalMapper;
import com.lyz.shop.model.CapitalEntity;
import com.lyz.shop.service.CapitalService;
import org.springframework.stereotype.Service;

@Service
public class CapitalServiceImpl extends ServiceImpl<CapitalMapper, CapitalEntity> implements CapitalService {
}
